if __name__ == '__main__':
	import sys
	import os

	if os.path.abspath ('./assistant') not in sys.path:
		sys.path.append (os.path.abspath ('./assistant'))
		
	import assistant

	assistant.run ()