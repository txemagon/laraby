import sys
import os

if os.path.abspath ('../../required') not in sys.path:
	sys.path.append (os.path.abspath ('../../required'))

import datetime
import platform
import random
import required.assistant_error
import required.lista_en_orden
import required.listen_talk

class Fecha (object):

	def __init__ (self, formato:int = 12, lang:str = 'es'):
		slash = '/' if platform.system () != 'Windows' else '\\'

		with open (f'.{slash}/{slash}config{slash}lang_info.json', 'r', encoding = 'utf-8') as l_info:
			lang_info = json.load (l_info)

		self.lang = lang_info [lang]
		self.comunicacion = required.listen_talk.Listen_Talk ()

		try:
			numero = int(open (os.path.abspath (f'.{slash}modules{slash}fechas{slash}format.conf'), 'r').read ())
			self.formato = formato
		except Exception as e:
			raise required.assistant_error.Assistant_Error (f'Error inesperado en {os.path.abspath (os.path.dirname (__file__))}{slash}main.py, consulta el log.')

	def assistant_update_lang (lang:str = 'es'):
		self.lang = lang

	def decir_fecha (self, comando:str, palabras:dict):
		pass

	def decir_hora (self, comando:str, palabras:dict):
		hora = ''
		if datetime.datetime.now ().minute == 15:
			if self.formato == 12:
				hora = f'{self.__12horas (datetime.datetime.now ().hour)} y cuarto'
			else:
				hora = f'{self.__24horas (datetime.datetime.now ().hour)} y cuarto'

		elif datetime.datetime.now ().minute == 30:
			if self.formato == 12:
				hora = f'{self.__12horas (datetime.datetime.now ().hour)} y media'
			else:
				hora = f'{self.__24horas (datetime.datetime.now ().hour)} y media'

		elif datetime.datetime.now ().minute == 45:
			if self.formato == 12:
				hora = f'{self.__12horas (datetime.datetime.now ().hour + 1)} menos cuarto'
			else:
				hora = f'{self.__24horas (datetime.datetime.now ().hour + 1)} menos cuarto'

		else:
			hora = datetime.datetime.now ().strftime ('%H y %M')

		if required.lista_en_orden.lista_en_orden (comando, ['en el reloj'], lang = self.lang):
			self.comunicacion.talk ([f'En el reloj pone que son las {hora}', f'En el reloj pone que son las {hora} minutos'][random.randint (0, 1)], self.lang)
		else:
			self.comunicacion.talk ([f'Las {hora}', f'Son las {hora}', f'Van siendo las {hora}'][random.randint (0, 2)], lang = self.lang)

	def decir_fecha_hora (self, comando:str, palabras:dict):
		if random.randint (0, 1) == 0:
			self.decir_fecha (comando)
			self.decir_hora (comando)
		else:
			self.decir_hora (comando)
			self.decir_fecha (comando)

	def cambio_formato (self, comando:str, palabras:dict):
		self.comunicacion.talk ([f'Me encuentro en formato {self.config ["formato_hora"]} horas', f'Actualmente me encuentro en formato {self.config ["formato_hora"]} horas', f'Estoy en formato {self.config ["formato_hora"]} horas'][random.randint (0, 2)], lang = self.lang)
		self.comunicacion.talk ('A que formato quieres cambiar?', lang = self.lang)
		orden = self.comunicacion.listen ()

		if required.lista_en_orden.lista_en_orden (orden, ['12', 'doce'], lang = self.lang):
			self.formato = 12

		if required.lista_en_orden.lista_en_orden (orden, ['24', 'veinticuatro', 'veinte y cuatro', 'veinti cuatro'], lang = self.lang):
			self.formato = 24

		self.__guardar_config ()
		self.comunicacion.talk (['Hecho', 'Prefecto, hecho', 'Vale, ya esta', 'Ya', 'Finalizado', 'Vale', 'Vale perfecto'][random.randint (0, 7)], lang = self.lang)

	def __guardar_config (self):
		slash = '/' if platform.system () != 'Windows' else '\\'
		open (f'.{slash}format.conf', 'w').write (self.formato)

	def __12horas (self, hora:int):
		return hora % 12

	def __24horas (self, hora:int):
		return hora % 24


if __name__ == '__main__':
	fecha = Fecha ()
