from . import modules
from . import required
import colorama
import json
import platform
import os
import signal
import subprocess
import time
import threading

__all__ = ['modules', 'required', 'run', 'signal_manager', 'evento_salir']


evento_salir = threading.Event ()


def signal_manager (sigint, funcion):
	evento_salir.set ()


def run ():
	colorama.init (autoreset = True)


	'''
	# ===============================================================================
	# =           Todo esto se tendrá que sustituir por una UI simple plz           =
	# ===============================================================================
	'''
	print (f'{colorama.Fore.GREEN}Iniciando...')


	'''
	# ===============================================================
	# =           Carga la configuración desde el archivo           =
	# ===============================================================
	'''

	CONFIG = {}
	try:
		with open ('./assistant/config/general_info.json', 'r', encoding = 'utf-8') as general_info:
			CONFIG = json.load (general_info)

	except Exception as e:
		slash = '/' if platform.system () != 'Windows' else '\\'
		raise required.assistant_error.Assistant_Error (f'No se ha podido cargar el archivo {os.path.abspath (f"./config/general_info.json")} en {os.path.abspath (os.path.dirname (__file__))}{slash}__init__.py')

		
	'''
	# =======================================================
	# =           Estructura general del programa           =
	# =======================================================
	# Se crearán tres hilos de ejecución (será necesario un =
	# semáforo), uno estará escuchando y obedeciendo, el    =
	# segundo será para recordatorios y el tercero será el  =
	# servidor del gestor web.                              =
	# =======================================================
	# UPDATE:                                               =
	# Cuando alguien cree un modulo, si lo necesita,        =
	# podrá crear un hilo de ejecución                      =
	# =======================================================
	'''

	signal.signal (signal.SIGINT, signal_manager)
	# Hilo de ordenes
	hilo_accion       = threading.Thread (name = 'Hilo principal', target = required.action_thread.action_thread, args = (CONFIG, ))
	# Hilo de recordatorios
	hilo_recordatorio = threading.Thread (name = 'Hilo de recordatorios', target = required.reminder_thread.reminder_thread, args = (CONFIG, ))
	# Hilo de interfaz gráfica
	hilo_gui          = threading.Thread (name = 'Hilo de interfaz gráfica', target = required.gui_thread.gui_thread)

	try:
		# Inicia el hilo de las ordenes
		hilo_accion.start ()
		# Inicia el hilo de los recordatorios
		hilo_recordatorio.start ()
		# Inicia la interfaz grafica
		hilo_gui.start ()
		# Enciende del servidor del gestor web
		hilo_web_server = subprocess.Popen (['python', f'{os.path.abspath ("./assistant/required/web_app.py")}'])

	except Exception as e:
		'''
		# ================================================================
		# =           Si hay cualquier error, para el programa           =
		# ================================================================
		'''
		print (f'{colorama.Fore.RED}Error')
		hilo_accion.join ()
		hilo_recordatorio.join ()
		hilo_gui.join ()
		hilo_accion.stop ()
		hilo_recordatorio.stop ()
		hilo_gui.stop ()
		os.kill (hilo_web_server.pid, signal.SIGUSR1)
		os._exit (1)
