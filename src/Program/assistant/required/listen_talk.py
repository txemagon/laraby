import colorama
import google_trans_new
import gtts
import json
import os
import playsound
import re
import speech_recognition
import unicodedata

colorama.init (autoreset = True)

def get_langs ():
	with open ('./assistant/config/lang_info.json', 'r', encoding = 'utf-8') as general_info:
		return json.load (general_info)

class Listen_Talk (object):

	VOL_WHISPER = 0.3
	VOL_NORMAL = 0.65
	VOL_SCREAM = 1
	LANGS = {'es': {'voice': 'es', 'recognizer': 'es-es'}, 'en': {'voice': 'en', 'recognizer': 'en-us'}}
	SLOW = True

	def __init__ (self):
		self.reconocedor = speech_recognition.Recognizer ()
		self.traductor = google_trans_new.google_translator ()

		with open ('./assistant/config/general_info.json', 'r', encoding = 'utf-8') as general_info:
			self.config = json.load (general_info)

	def listen (self, lang:str = 'es'):
		Listen_Talk.LANGS [lang]['recognizer']
		orden = ''
		with speech_recognition.Microphone () as sonido:
			if self.config ['state'] != f'En reposo (Di {self.config ["assistant_name"]} para despertarme).':
				self.config ['state'] = 'Activo (Escuchando)'

			self.__guardar ()
			print (f'{colorama.Fore.YELLOW}Escuchando...')
			self.reconocedor.adjust_for_ambient_noise (sonido)
			audio = self.reconocedor.listen (sonido)

			try:
				if self.config ['state'] != f'En reposo (Di {self.config ["assistant_name"]} para despertarme).':
					self.config ['state'] = 'Activo (Reconociendo)'

				self.__guardar ()
				print (f'{colorama.Fore.YELLOW}Reconociendo...')
				orden = self.reconocedor.recognize_google (audio, language = Listen_Talk.LANGS [lang]['recognizer'])
				orden = self.__acent (orden)
				print (f'{colorama.Fore.CYAN}Has dicho: {orden.lower ()}.\n')
				
			except Exception as e:
				print (f'{colorama.Fore.RED}Error al reconocer el audio.')

		return orden

	def talk (self, text:str, lang:str, slow:bool = False): # , vol:float = 0.65
		self.config ["state"] = 'Activo (Hablando)'
		self.__guardar ()
		if lang != self.traductor.detect (text):
			text = self.traductor.translate (text, lang_tgt = Listen_Talk.LANGS [lang]['voice'])

		nombre_archivo = './audio.mp3'
		if os.path.exists (nombre_archivo):
			os.remove (nombre_archivo)
			
		audio = gtts.gTTS (text = text, lang = Listen_Talk.LANGS [lang]['voice'])
		audio.save (nombre_archivo)
		playsound.playsound (nombre_archivo)

		self.config ["state"] = 'Activo'
		self.__guardar ()

	def __acent(self, texto:str):
		texto = re.sub (r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", unicodedata.normalize ('NFD', texto), 0, re.I)
		texto = unicodedata.normalize ('NFC', texto)
		return texto

	def __guardar (self):
		with open ('./assistant/config/general_info.json', 'w', encoding = 'utf-8') as conf:
			json.dump (self.config, conf, indent = 4, ensure_ascii = False)
			conf.close ()