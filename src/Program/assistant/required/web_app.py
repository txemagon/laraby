import flask
import json
import os
import requests
import socket

app = flask.Flask (__name__)
VERSION = 'v1'

@app.route ('/')
def home ():
	return flask.render_template (f'{VERSION}/home.html')

@app.route ('/acerca-de')
def acerca_de ():
	return flask.render_template (f'{VERSION}/acercade.html')

@app.context_processor
def get_nombre ():
	# if __name__ == '__main__':
	# 	ruta = '../config/general_info.json'
	# else:
	# 	ruta = './assistant/config/general_info.json'
		
	with open ('./assistant/config/general_info.json', 'r', encoding = 'utf-8') as archivo:
		config = json.load (archivo)
		nombre = config ['assistant_name']
		archivo.close ()
		return dict (get_nombre = nombre.capitalize ())
'''
@app.route ('/install')
def install ():
	module_file = flask.request.args.get ('module_id') + '.py'
	print ('module_id:', module_file)
	url = 'http://81.40.104.123/myassistant/' + module_file
	print ('url: ', url)
	info = requests.get (url)
	open (f'../program/modules/module_{module_file}', 'wb').write (info.content)
	return flask.render_template ('python_script.html')
'''
'''
@app.route ('/encender')
def encender ():
	pass
'''
@app.route ('/cambio-de-nombre')
def cambiar_nombre ():
	return flask.render_template (f'{VERSION}/cambio_de_nombre.html')

def run ():
	print ('Inicia web_app')
	app.run (host = '0.0.0.0', debug = True)
	# aruba

if __name__ == '__main__':
	run ()