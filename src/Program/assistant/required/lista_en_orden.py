import google_trans_new
import re

# def lista_en_orden (query:str, lista:list, lang:str, y:list = [], o:list = [], no:list = []):
# 	traductor = google_trans_new.google_translator ()

#	if lang != traductor.detect (query):
#		query = traductor.translate (query, lang_tgt = lang)

#		for item in lista:
#			item = traductor.translate (item, lang_tgt = lang)

#		if len (y) > 0:
#			for item in y:
#				item = traductor.translate (item, lang_tgt = lang)

#		if len (no) > 0:
#			for item in no:
#				item = traductor.translate (item, lang_tgt = lang)

# 	print ('Lista a comparar:', lista)
			
# 	for item in lista:
# 		if len (y) == 0 and len (o) == 0 and len (no) == 0:
# 			if item in query:
# 				return True

# 		elif len (y) != 0 and len (o) == 0 and len (no) == 0:
# 			for i in y:
# 				if item in query and i in query:
# 					return True

# 		elif len (y) == 0 and len (o) != 0 and len (no) == 0:
# 			for _o in o:
# 				if item in query and _o in query:
# 					return True

# 		elif len (y) == 0 and len (o) == 0 and len (no) != 0:
# 			for _not in no:
# 				if item in query and not (_not in query):
# 					return True

# 		elif len (y) != 0 and len (o) == 0 and len (no) != 0:
# 			for i in y:
# 				for _not in no:
# 					if item in query and i in query and not (_not in query):
# 						return True

# 		elif len (y) != 0 and len (o) != 0 and len (no) == 0:
# 			for i in y:
# 				for _o in o:
# 					if item in query and i in query or item in query and _o in query:
# 						return True

# 		elif len (y) == 0 and len (o) != 0 and len (no) != 0:
# 			for _o in o:
# 				for _not in no:
# 					if item in query and _or in query and item in query and not (_not in query):
# 						return True

# 		elif len (y) != 0 and len (o) != 0 and len (no) != 0:
# 			for i in y:
# 				for _not in no:
# 					for _o in o:
# 						if (item in query and i in query) and (item in query or _o in query) and not (_not in query):
# 							return True

# 	return False

# def lista_en_orden2 (query:str, lista:list, lang:str, y:list = [], no:list = [], verbose:bool = False):
# 	traductor = google_trans_new.google_translator ()

# 	'''
# 	# ===========================================================
# 	# =    Traduzco todo al idioma destino si no son iguales    =
# 	# ===========================================================
# 	'''
# 	try:
# 		if verbose:
# 			print ('  Lang:', lang)
# 			print ('Detect:', traductor.detect (query) [0])

# 		if lang != traductor.detect (query) [0]:
# 			query = traductor.translate (query, lang_tgt = lang)

# 			for item in lista:
# 				item = traductor.translate (item, lang_tgt = lang)

# 			if len (y) > 0:
# 				for item in y:
# 					item = traductor.translate (item, lang_tgt = lang)

# 			if len (no) > 0:
# 				for item in no:
# 					item = traductor.translate (item, lang_tgt = lang)

# 	except Exception:
# 		return False

# 	if verbose:
# 		print ('     Lista a comparar:', lista)
# 		print (' Lista (y) a comparar:', y)
# 		print ('Lista (no) a comparar:', no)

# 	'''
# 	# ============================================================================
# 	# =  Creo las expresiones regulares que se necesitan a partir de las listas  =
# 	# ============================================================================
# 	'''
# 	lista = '^((?=.*\\b' + '\\b)|(?=.*\\b'.join (lista) + '\\b)).*$'
# 	y     = '^((?=.*\\b' + '\\b)|(?=.*\\b'.join (y)     + '\\b)).*$' if len (y)  > 0 else ''
# 	no    = '^((?=.*\\b' + '\\b)|(?=.*\\b'.join (no)    + '\\b)).*$' if len (no) > 0 else ''

# 	lista = lista.replace (' ', '\\s')
# 	y     =     y.replace (' ', '\\s')
# 	no    =    no.replace (' ', '\\s')

# 	if verbose:
# 		print ('lista:', lista)
# 		print ('    y:', y)
# 		print ('   no:', no)

# 	'''
# 	# =====================================
# 	# =    Comparo los datos recojidos    =
# 	# =====================================
# 	'''
# 	if y == '' and no == '':
# 		if re.match (lista, query):
# 			return True

# 	elif y != '' and no == '':
# 		if re.match (lista, query) and re.match (y, query):
# 			return True

# 	elif y == '' and no != '':
# 		if re.match (lista, query) and not (re.match (no, query)):
# 			return True

# 	else: # resultado_y != '' and resultado_no != ''
# 		if re.match (lista, query) and re.match (y, query) and not (re.match (no, query)):
# 			return True

# 	return False

def lista_en_orden (query:str, lista:list, lang:str, y:list = [], no:list = [], verbose:bool = False):

	'''
	# =================================================================================
	# =  Relleno un diccionario con los valores de las palabras entre corchetes '[]'  =
	# =================================================================================
	# = Las claves serán las palabras contenidas entre los corchetes y los valores    =
	# = serán las palabras que diga el usuario entre las palabras colindantes de '[]' =
	# =================================================================================
	# = Ejemplo:                                                                      =
	# =     Entrada:                                                                  =
	# =         - 'Hola soy Txema, que tal'                                           =
	# =     Frase modelo a comparar:                                                  =
	# =         - 'Hola soy [nombre], que tal'                                        =
	# =     Diccionario retornado (ret):                                              =
	# =         - {'nombre': 'Txema'}                                                 =
	# =================================================================================
	'''
	ret = {}
	indices = [] # Indices del diccionario (ret)
	valores = [] # Valores del diccionario (ret)
	for item in lista:
		'''
		# ======================================================================================
		# =            Busco ocurrencias de todas las palabras entre corchetes ([])            =
		# ======================================================================================
		'''
		ocurrencias = re.findall (r'\[(.*?)\]', item)

		if verbose:
			print ('ocurrencias:', ocurrencias)

		'''
		# =========================================================
		# =            Meto las ocurrencias en indices            =
		# =========================================================
		# = No podía hacer 'indices = list (set (ocurrencias))'   =
		# = porque desordenaba el contenido.                      =
		# =========================================================
		'''
		for occ in ocurrencias:
			if occ not in indices:
				indices.append (occ)

	if verbose:
		print ('indices:', indices)

	'''
	# ===============================================================================
	# =         Elijo cuál de los items de la lista es el que ha coincidido         =
	# =         con la frase del usuario en caso de que haya palabras clave         =
	# ===============================================================================
	'''
	if len (indices) > 0:
		for item in lista:
			se_ha_encontrado_coincidencia = False
			for indice in range (len (indices)):
				indices [indice] = f'[{indices [indice]}]'

			'''
			# ====================================================================================================
			# =       Con éstas lineas de abajo, consigo una lista de los puntos donde hay palabras clave.       =
			# ====================================================================================================
			# = En la última de las 4 quito los posibles espacios sobrantes antes, y después de cada resultado.  =
			# ====================================================================================================
			'''
			res = '|'.join (indices).replace ('[', '\\[').replace (']', '\\]')
			res = re.split (res, item)
			for i, resultado in enumerate (res):
				res [i] = res [i].strip ()

			'''
			# ============================================================================================
			# =       Creo el regex que devolverá las partes de la frase que no son palabras clave       =
			# ============================================================================================
			'''
			res = '(.*)' + '\\s(.*)\\s'.join (res) + '(.*)'

			valores = re.match (res, query)
			try:
				valores = list (valores.groups ())
				for i, resultado in enumerate (valores):
					valores [i] = valores [i].strip ()

				se_ha_encontrado_coincidencia = True
			except:
				'''
				# ================================================================
				# =       Éste es el caso en el que re.match () ha fallado       =
				# =       y el retorno es 'None' en vez de 'Match'.              =
				# ================================================================
				'''
				pass

			'''
			# =============================================================================
			# =       Introduzco dos índices que podrían ser útiles para el usuario       =
			# =============================================================================
			# = 1.- [order_start]: Contiene la parte de la frase input que sobresale por  =
			# =                    delante de la frase modelo del usuario.                =
			# = 2.- [order_end]:   Contiene la parte de la frase input que sobresale por  =
			# =                    detrás de la frase modelo del usuario.                 =
			# =============================================================================
			'''
			indices.insert (0, '[order_start]')
			indices.append ('[order_end]')
			# En este punto ya tengo tanto claves como valores
			if verbose:
				print ('218: valores:', valores)
				print ('219: indices:', indices)
			'''
			# ================================================================
			# =            Introduzco los datos en el diccionario            =
			# ================================================================
			'''
			for i in range (len (valores)): # Podría perfectamente ser indices, se supone que en este punto miden lo mismo
				ret [indices [i][1:-1]] = valores [i]
			
			'''
			# =========================================================================
			# =            Si se han encotrado coincidencias en las frases            =
			# =========================================================================
			# = Sale del for. ¿Por que? Porque significa que sea cual sea el código   =
			# = de abajo, la frase seleccionada es la que se ha procesado en esta     =
			# = iteración.                                                            =
			# =========================================================================
			'''
			if se_ha_encontrado_coincidencia:
				break

	if verbose:
		print (ret)

	'''
	# ===========================================================
	# =    Traduzco todo al idioma destino si no son iguales    = MIRAR! QUE PARECE QUE CON LO DE LOS DICCIONARIOS NO FUNCIONA
	# ===========================================================
	'''
	traductor = google_trans_new.google_translator ()
	try:
		if verbose:
			print ('  Lang:', lang)
			print ('Detect:', traductor.detect (query) [0])

		if lang != traductor.detect (query) [0]:
			query = traductor.translate (query, lang_tgt = lang)

			for item in lista:
				item = traductor.translate (item, lang_tgt = lang)

			if len (y) > 0:
				for item in y:
					item = traductor.translate (item, lang_tgt = lang)

			if len (no) > 0:
				for item in no:
					item = traductor.translate (item, lang_tgt = lang)

	except Exception:
		return False

	if verbose:
		print ('     Lista a comparar:', lista)
		print (' Lista (y) a comparar:', y)
		print ('Lista (no) a comparar:', no)

	'''
	# =====================================================================================
	# =       En caso de que haya palabras clave, las reemplazo por el regex: (.*?)       =
	# =====================================================================================
	# = 1.- Si no hay palabras clave, este bucle no se ejecutará.                         =
	# = 2.- La expresion regular '(.*?)' significa 'Cualquier numero de caracteres de     =
	# = cualquier caracter'.                                                              =
	# =====================================================================================
	'''
	for i, item in enumerate (lista):
		for r, replace in enumerate (indices):
			lista [i] = lista [i].replace (replace, '(.*?)')

	'''
	# ============================================================================
	# =  Creo las expresiones regulares que se necesitan a partir de las listas  =
	# ============================================================================
	'''
	lista = r'^((?=.*\b' + r'\b)|(?=.*\b'.join (lista) + r'\b)).*$'
	y     = r'^((?=.*\b' + r'\b)|(?=.*\b'.join (y)     + r'\b)).*$' if len (y)  > 0 else ''
	no    = r'^((?=.*\b' + r'\b)|(?=.*\b'.join (no)    + r'\b)).*$' if len (no) > 0 else ''


	lista = lista.replace (' ', '\\s')
	y     =     y.replace (' ', '\\s')
	no    =    no.replace (' ', '\\s')

	if verbose:
		print ('lista:', lista)
		print ('    y:', y)
		print ('   no:', no)

	'''
	# =======================================================================
	# =    Comparo los datos recojidos y retorno las palabras necesarias    =
	# =======================================================================
	'''
	if y == '' and no == '':
		if re.match (lista, query):
			return (ret if len (ret) > 0 else True)

	elif y != '' and no == '':
		if re.match (lista, query) and re.match (y, query):
			return (ret if len (ret) > 0 else True)

	elif y == '' and no != '':
		if re.match (lista, query) and not (re.match (no, query)):
			return (ret if len (ret) > 0 else True)

	else: # resultado_y != '' and resultado_no != ''
		if re.match (lista, query) and re.match (y, query) and not (re.match (no, query)):
			return (ret if len (ret) > 0 else True)

	return False

if __name__ == '__main__':
	# res = lista_en_orden (query = 'Hola laraby', lista = ["laraby", "larabi", "lara by", "lara bi", "laravy", "laravi", "lara vy", "lara vi"], lang = 'es', verbose = False)
	# res = lista_en_orden (query = 'laraby por favor exit si lo deseas', lista = ['salir', 'exit', 'apagar', 'apagate', 'desconectar', 'desconectate', 'adios'], lang = 'es', no = ['exito'], verbose = True)
	# print ('res:', res)
	# Regex final (En el primer caso):
	# Lista: r'^((?=.*\blaraby\b)|(?=.*\blarabi\b)|(?=.*\blara\sby\b)|(?=.*\blara\sbi\b)|(?=.*\blaravy\b)|(?=.*\blaravi\b)|(?=.*\blara\svy\b)|(?=.*\blara\svi\b)).*$'

	# Regex final (En el segundo caso):
	# Lista: r'^((?=.*\bsalir\b)|(?=.*\bexit\b)|(?=.*\bapagar\b)|(?=.*\bapagate\b)|(?=.*\bdesconectar\b)|(?=.*\bdesconectate\b)|(?=.*\badios\b)).*$'
	#    No: r'^((?=.*\bexito\b)).*$'
	dic = lista_en_orden (query = 'Hola tio Diego que tal soy Laraby que tal estas', lista = ['[nombre] que [tal] soy [asistente] que', 'soy [asistente] que tal [nombre]'], lang = 'es', verbose = False)
	print (dic)

	dic = lista_en_orden (query = 'E Hola Diego que tal soy Laraby que pasha tio', lista = ['que tal soy Laraby'], lang = 'es', verbose = False)
	print (dic)

	dic = lista_en_orden (query = 'Hola que tal', lista = ['soy laraby', 'soy diego'], lang = 'es', verbose = False)
	print (dic)