import sys
import os

# if os.path.abspath ('../modules') not in sys.path:
# 	sys.path.append (os.path.abspath ('../modules'))

if os.path.abspath ('../../assistant') not in sys.path:
	sys.path.append (os.path.abspath ('../../assistant'))

import assistant
import json
from . import lista_en_orden
from . import listen_talk
# import modules
import random
import socket
import webbrowser

def guardar (config):
	with open ('./assistant/config/general_info.json', 'w', encoding = 'utf-8') as conf:
		json.dump (config, conf, indent = 4, ensure_ascii = False)
		conf.close ()

def action_thread (config):
	comunicacion = listen_talk.Listen_Talk ()
	print ('Inicia action_thread')
	while True:
		if assistant.evento_salir.is_set ():
			os._exit (0)

		entendido = False
		cuenta_entendido = 0
		command = comunicacion.listen ()
		if lista_en_orden.lista_en_orden (query = command, lista = config ['wake_word'], lang = config ['lang']):
			for levantar in config ['wake_word']:
				command = command.replace (levantar, '')

			while not (entendido):
				if cuenta_entendido > 0:
					command = comunicacion.listen ()
					cuenta_entendido = 0

				if assistant.evento_salir.is_set ():
					os._exit (0)

				entendido = True

				'''
				# ============================================================================
				# =           Comando para abrir la web de configuración de Laraby           =
				# ============================================================================
				# = Abre la la web de Laraby para poder configurarlo                         =
				# ============================================================================
				'''
				if words := lista_en_orden.lista_en_orden (query = command, lista = ['abre tu web', 'abre tu sitio web', 'abrir tu web', 'abrir tu sitio web'], lang = config ['lang']):
					webbrowser.open (f'http://{socket.gethostname ()}:5000/')
					comunicacion.talk (['Hecho', 'Prefecto, hecho', 'Vale, ya esta', 'Ya', 'Finalizado', 'Vale', 'Vale perfecto'][random.randint (0, 6)], lang = config ['lang'])
				
					'''
					# ====================================================
					# =           Comando para apagar a Laraby           =
					# ====================================================
					# No sale, solo pone el asistente en modo reposo,    =
					# cuando oiga su wake word volverá a escuchar        =
					# ====================================================
					'''
				elif words := lista_en_orden.lista_en_orden (query = command, lista = ['salir', 'exit', 'apagar', 'apagate', 'desconectar', 'desconectate', 'adios'], lang = config ['lang'], no = ['exito']):
					comunicacion.talk (['Adios', 'Hasta luego', 'adios adios', 'Venga, hasta luego', f'Venga, adios'][random.randint (0, 4)], lang = config ['lang'])
					# Cambia el estado
					config ['state'] = f'En reposo (Di {config ["assistant_name"]} para despertarme).'
					guardar (config)
					# Empieza a solo escuchar alguna de las wake_word
					while True:
						command = comunicacion.listen ()
						if assistant.evento_salir.is_set ():
							os._exit (0)

						if words := lista_en_orden.lista_en_orden (query = command, lista = config ['wake_word'], lang = config ['lang']):
							comunicacion.talk (['Ya estoy', 'He vuelto', 'Me reclaman', 'Peligro, peligro'][random.randint (0, 3)], lang = config ['lang'])
							config ['state'] = 'Activo'
							guardar (config)
							break

					'''
					# ===================================================================
					# =           Comando para cambiar de idioma el asistente           =
					# ===================================================================
					'''
				elif words := lista_en_orden.lista_en_orden (query = command, lista = ['cambia el idioma', 'cambiar idioma', 'cambia tu idioma', 'cambia el lenguaje', 'cambiar lenguaje', 'cambia tu lenguaje'], lang = config ['lang']):
					if words := lista_en_orden.lista_en_orden (query = command, lista = ['a español'], lang = config ['lang']):
						config ['lang'] = 'es'

					if words := lista_en_orden.lista_en_orden (query = command, lista = ['a ingles'], lang = config ['lang']):
						config ['lang'] = 'en'

					guardar (config)
					comunicacion.talk (['Hecho', 'Prefecto, hecho', 'Vale, ya esta', 'Ya', 'Finalizado', 'Vale', 'Vale perfecto'][random.randint (0, 6)], lang = config ['lang'])

				elif words := lista_en_orden.lista_en_orden (query = 'Hola Diego que tal', lista = ['Hola [nombre]'], lang = config ['lang']):
					print ('words:', words)
				else:
					comunicacion.talk (['No te he entendido', 'Que dices? No entiendo', 'No entiendo', 'No te entiendo cariño'][random.randint (0, 3)], lang = config ['lang'])
					entendido = False
					cuenta_entendido += 1