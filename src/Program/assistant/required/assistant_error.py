import datetime
import os
import platform
import pyautogui
import traceback


# ===========================================================
# =                  Clase Assistant_Error                  =
# ===========================================================
# Es una clase que cierra el asistente en caso de error
# Hace 2 cosas:
# 
#  1 - Muestra un cuadro de alerta en la pantalla indicando
#      la ruta del archivo causante del error.
# ===========================================================

class Assistant_Error (Exception):

	def __init__(self, mensaje):
		# super (Assistant_Error, self).__init__ (mensaje)
		slash = '/' if platform.system () != 'Windows' else '\\'
		with open (f'./assistant/logs/log_{datetime.datetime.now ().strftime("%Y-%m-%d")}_Fecha.log', 'a+') as f: f.write (f'{datetime.datetime.now ().strftime("%Y-%m-%d")} ({datetime.datetime.now ().strftime("%H:%M:%S")}): {super ()}\n\n{traceback.format_exc ()}\n\n\n\n')
		pyautogui.alert (text = mensaje)
		os._exit (1)

# ======  End of Clase Assistant_Error  =======