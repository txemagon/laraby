import sys
import os

if os.path.abspath ('../../modules') not in sys.path:
	sys.path.append (os.path.abspath ('../modules'))

if os.path.abspath ('../../../assistant') not in sys.path:
	sys.path.append (os.path.abspath ('../../assistant'))

import assistant
import datetime
import json
from . import listen_talk
import platform
import random
import time

if platform.system () == 'Windows':
	import pythoncom

def reminder_thread (config):
	if platform.system () == 'Windows':
		pythoncom.CoInitialize ()
		
	comunicacion = listen_talk.Listen_Talk ()
	print ('Inicia reminder_thread')
	while True:
		if assistant.evento_salir.is_set ():
			os._exit (0)

		'''
		# =========================================
		# =            Array "alarmas"            =
		# =========================================
		# Contiene la información con todas las   =
		# alarmas en un diccionario que trae      =
		# desde el archivo alarmas.json           =
		# =========================================
		'''
		alarmas = []

		'''
		# ===============================================
		# =            Cargo el archivo json            =
		# ===============================================
		'''
		with open ('./assistant/required/reminders/alarmas.json', 'r', encoding = 'utf-8') as alarms:
			alarmas = json.load (alarms)

		'''
		# ====================================================
		# =            Recojo fecha y hora actual            =
		# ====================================================
		'''
		ahora = datetime.datetime.now ()

		'''
		# ===========================================================================================
		# =            Por cada alarma verifico si tengo que lanzarla o no según el tipo            =
		# ===========================================================================================
		'''
		for alarma in alarmas:
			if alarma ['tipo'] == 'diario':
				if ahora.hour == (alarma ['hora'] * (1 if alarma ['meridian'] == 'am' else 2)) and ahora.minute == alarma ['minuto']:
					if alarma ['musica']['?']:
						# Se ejecuta musica de un artista
						print ('Musica')
					else:
						# Se ejecuta una melodia
						print ('Melodia')

			elif alarma ['tipo'] == 'semanal':
				if alarma ['dia'] == ahora.weekday ():
					if ahora.hour == (alarma ['hora'] * (1 if alarma ['meridian'] == 'am' else 2)) and ahora.minute == alarma ['minuto']:
						if alarma ['musica']['?']:
							# Se ejecuta musica de un artista
							print ('Musica')
						else:
							# Se ejecuta una melodia
							print ('Melodia')

			elif alarma ['tipo'] == 'mensual':
				if alarma ['dia'] == ahora.day:
					if ahora.hour == (alarma ['hora'] * (1 if alarma ['meridian'] == 'am' else 2)) and ahora.minute == alarma ['minuto']:
						if alarma ['musica']['?']:
							# Se ejecuta musica de un artista
							print ('Musica')
						else:
							# Se ejecuta una melodia
							print ('Melodia')

		'''
		# ====================================================
		# =            Espero al minuto siguiente            =
		# ====================================================
		'''
		time.sleep (60)