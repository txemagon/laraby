import numpy
import pygame
import sounddevice
import win32api
import win32con
import win32gui

class GUI_Thread (object):
	"""docstring for GUI_Thread"""
	def __init__ (self):
		self.radio = 20

	def radio_from_volume (self, indata, outdata, frames, time, status):
		volume_norm = numpy.linalg.norm (indata) * 10
		# print (volume_norm)
		self.radio = 20 + int (volume_norm)

	def run (self):
		self.radio = 20
		pygame.init ()

		screen = pygame.display.set_mode ((0, 0), pygame.NOFRAME) # For borderless, use pygame.NOFRAME
		salir = False
		fuchsia = (255, 0, 128)  # Transparency color
		naranja_laraby = (255, 124, 0, 0)

		# Create layered window
		hwnd = pygame.display.get_wm_info () ["window"]
		win32gui.SetWindowLong (hwnd, win32con.GWL_EXSTYLE, win32gui.GetWindowLong (hwnd, win32con.GWL_EXSTYLE) | win32con.WS_EX_LAYERED)
		# Set window transparency color
		win32gui.SetLayeredWindowAttributes (hwnd, win32api.RGB (*fuchsia), 0, win32con.LWA_COLORKEY)

		with sounddevice.Stream (callback = self.radio_from_volume):
			while not salir:
				# print (self.radio)
				for event in pygame.event.get ():
					if event.type == pygame.QUIT:
						salir = True

					if event.type == pygame.KEYDOWN:
						if event.key == pygame.K_q:
							salir = True

				screen.fill (fuchsia)  # Transparent background
				pygame.draw.circle (screen, naranja_laraby, (70, 70), self.radio, 0)
				# pygame.draw.rect(screen, naranja_laraby, pygame.Rect(30, 30, 60, 60))
				pygame.display.update ()

		pygame.quit ()
		

def gui_thread ():
	hilo = GUI_Thread ()
	hilo.run ()

if __name__ == '__main__':
	gui_thread ()