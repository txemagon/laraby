window.onload = function () {
	// body...
	// setInterval (actualizarEstado, 1000)
	actualizarTiempo ()
	setInterval (actualizarTiempo, 500)
}

function actualizarEstado () {

	var req = new XMLHttpRequest ()

	req.onreadystatechange = function () {
		if (req.readyState == 4 && req.status == 200) {
			document.getElementById ('chat').innerHTML = req.responseText
		}
	}

	req.open ('GET', 'chat.php', true)
	req.send ()
}

function actualizarTiempo () {
	var dias_semana = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']
	var meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
	var fecha = new Date ()
	var periodo = ((fecha.getHours () - 12) < 1) ? 'am' : 'pm'
	var hora = 0
	hora = (fecha.getHours () % 12 == 0 && periodo == 'am') ? 12 : fecha.getHours () % 12

	document.getElementById ('label_fecha').innerHTML = dias_semana [fecha.getDay ()] + ', ' + fecha.getDate () + ' de ' + meses [fecha.getMonth ()] + ', ' + fecha.getFullYear ()
	document.getElementById ('label_hora').innerHTML  = hora + ':' + rellenaCeros (fecha.getMinutes (), 2) + ' ' + periodo
}

function rellenaCeros (num, longitud) {
	var string = num.toString ()
	while (string.length < longitud) {
		string = '0' + string
	}
	return string
}