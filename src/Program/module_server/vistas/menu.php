<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../estilos/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../estilos/bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="../estilos/main.css">
	<link rel="shortcut icon" type="image/png" href="../media/img/laraby1.png">
	<script type="text/javascript" src="../js/menu.js"></script>
	<title>Modulos de Laraby</title>
</head>
<body>
	<div class="row mih-100 w-100 p-0 m-0">
		<div class="col-xl-2 p-0 m-0 mibg-light mw-190">
			<div class="d-block w-100 my-5" align="center">
				<h1 class="text-light">Laraby</h1>
			</div>
			<div class="float-right w-100 p-0 m-0 mt-2">
				<div align="right" class="p-0">
					<a class="mibg-dark px-3 py-2 text-light btn btn-block menu-link" draggable="false" disabled>Home</a>
				</div>
				<div align="right" class="p-0">
					<a class="h-bg-lighter px-3 py-2 text-light btn btn-block menu-link" draggable="false">Acerca de...</a>
				</div>
				<div align="right" class="p-0">
					<a class="h-bg-lighter px-3 py-2 text-light btn btn-block menu-link" draggable="false">Buscar módulos</a>
				</div>
			</div>
			<!-- <button class="btn btn-danger px-3" style="font-size: 2em">&times;</button> -->
		</div>
		<div class="col-xl-10 p-0 m-0 mibg-dark">
			<div class="p-3">
				<div class="mibg-light mirounded px-5 py-6 text-light my-4">
					<h1 class="f-5" id="label_hora">Cargando...</h1>
					<h4 id="label_fecha">Cargando...</h4>
				</div>
			</div>
		</div>
	</div>
</body>
</html>