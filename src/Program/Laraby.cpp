#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	// Current working directory
	char cwd [PATH_MAX];
	char final_command [PATH_MAX];
	if (getcwd (cwd, sizeof (cwd)) != NULL) {
		// printf ("Current working dir: %s\n", cwd);
		strcpy (final_command, cwd);
		chdir ((const char *) cwd);
		strcat (final_command, "\\Python\\python.exe ");
		// printf ("Python path: %s\n", final_command);
		strcat (final_command, cwd);
		strcat (final_command, "\\__main__.py");
		// printf ("Final command: %s\n", final_command);
	} else {
		perror ("getcwd() error");
		return 1;
	}
	system ((const char *) final_command);
	return EXIT_SUCCESS;
}