#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	if (argc < 2) {
		printf ("El programa necesita saber que modulo instalar.\n");
		printf("Recuerda que puedes poner de parámetros\"-r [archivo]\" para instalar módulos de un fichero.\n");
		exit (1);
	}
	
	// Current working directory
	char cwd [PATH_MAX];
	char final_command [PATH_MAX];

	if (getcwd (cwd, sizeof (cwd)) == NULL) {
		perror ("getcwd() error");
		return 1;
	}
	// Comando de actualizar pip
	strcpy (final_command, cwd);
	chdir ((const char *) cwd);
	strcat (final_command, "\\Python\\python.exe -m pip install --upgrade pip");
	system ((const char *) final_command);
	// Comando de instalar el modulo
	// printf ("Current working dir: %s\n", cwd);
	strcpy (final_command, cwd);
	chdir ((const char *) cwd);
	strcat (final_command, "\\Python\\Scripts\\pip.exe install ");
	// printf ("Python path: %s\n", final_command);
	strcat (final_command, argv [1]);
	strcat (final_command, " --no-warn-script-location");
	printf ("Final command: %s\n", final_command);
	system ((const char *) final_command);
	return EXIT_SUCCESS;
}