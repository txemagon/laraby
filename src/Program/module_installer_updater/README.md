# Module Installer/Updater

### Intro

It's a program that installs modules in the assistant.
The interface of this program is **web**.

The program has to look at the `order.json` file to know what order to assign to each function and what python modules it requires.

The functions are saved in a file with the extension `.py`.

__*After running this program, the virtual assistant must be restarted.*__

### Required

- [x] The `assistant` folder must be added to the PYTHONPATH.

---

### Introducción

Es un programa que instala módulos en el asistente.
La interfaz de este programa es **web**.

Tiene que fijarse en el archivo `order.json` para saber qué orden asignar a cada función y qué modulos de python requiere.

Las funciones se guardan en un archivo con extensión `.py`.

__*Despues de ejecutarse este programa, el asistente virtual debe reiniciarse.*__

### Requerimientos

- [x] Se debe añadir la carpeta `assistant` al PYTHONPATH.